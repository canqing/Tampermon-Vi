// ==UserScript==
// @name         MV下载脚本
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Vi
// @match        https://bd.kuwo.cn/*
// @match        https://www.kugou.com/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @run-at       document-end
// @grant        GM_addElement
// @grant        GM_setClipboard
// @grant        GM_download
// @grant        GM_xmlhttpRequest
// @connect      bbs.tampermonkey.net.cn


// ==/UserScript==
//https://bd.kuwo.cn/singer_detail/630/mv

(function() {
    'use strict';

    let my_timer,mvname,altist,mvurl,mvinfo,Nourl=true
    var oldhref = location.href;
    start();

    setInterval(function () {

        if (location.href != oldhref || Nourl) {
            console.log("监听到地址变化!或者没有获取到地址："+location.href);
            oldhref = location.href;
            start();
        }
    }, 3000);
    /*停止定时器 clearInterval(my_timer); */

    function start(){
        //altist = "" ;
        //mvname = "" ;
        //mvurl = "" ;

        //酷我
        if (location.href.match("kuwo.cn/mvplay/") != null) {
            if ( document.querySelector("span.artist")){
                altist = document.querySelector("span.artist").innerText
            }
            if (document.querySelector("span.mv_name")){
                mvname = document.querySelector("span.mv_name").innerText
            }
            if (document.querySelector(".vjs-tech")){
                mvurl = document.querySelector(".vjs-tech").src
            }
            Nourl = false;
            GMadd(altist,mvname,mvurl,"kuwo",document.querySelector("div.container.sub_nav_out"));
        }
        //酷狗
        else if (location.href.match("www.kugou.com/mv") != null){
            if (document.querySelector("div.tabbarnew_mvTitle")){
                let title = document.querySelector("div.tabbarnew_mvTitle").innerText
                const parts = title.split('-');// 使用split方法按'-'分割字符串
                altist = parts[0].trim(); // 去除可能存在的空格
                mvname = parts[1].trim();
            }
            let aa = document.querySelector("#mvplayer")
            if (document.querySelector("#mvplayer")){
                mvurl = document.querySelector("#mvplayer").src

                if (mvurl.toLowerCase().includes('.mp4')) {
                    console.log("已经找到mp4的地址：" + mvurl);
                    Nourl = false;

                    GMadd(altist,mvname,mvurl,"kugou",document.querySelector("body > div.cmhead2"));
                }

            }
        }
        //酷狗跳转到MV
        else if (location.href.match("www.kugou.com/yy/html/search.html#searchType=song&searchKeyWord=") != null){
            document.querySelector("#mv").click()
            //alert("当前页面是歌曲搜索")
            Nourl = true;
        }


    }


    function GMadd(altist,mvname,mvurl,From,el){
        let BtnTexe,SearchUrl
        let Btnstyle = "background-color: #4CAF50;border: none;color: white;padding: 10px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;";

        if (From == "kugou"){
            SearchUrl = "https://www.kugou.com/yy/html/search.html#searchType=mv&searchKeyWord=";
        }
        else if (From == "kuwo"){
            SearchUrl = "https://bd.kuwo.cn/search/mv?key=";
        }

        if (!mvurl){BtnTexe = "MV的相关信息没有正确的获取到，请检查！！！"}
        else{
            BtnTexe = altist + " - " + mvname + " | " + mvurl;
        }

        const installArea = el

        //显示地址
        const murl = GM_addElement(installArea, 'a', {
            textContent: BtnTexe,
            href:mvurl,
            style:"display: block;background-color: #4CAF50;color: white;padding: 10px 32px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;",
        });

        //用Tampermonkey下载
        const TamDownBtn = GM_addElement(installArea, 'button', {
            textContent: " Tampermonkey下载",
            id:"TamDownBtn",
            style:Btnstyle

        });
        TamDownBtn.onclick = function DownB() {
            GM_download(mvurl,altist + " - " + mvname );
            document.querySelector("#TamDownBtn").textContent="Tamper正在下载..."
        }




        //复制： 歌手 - 名字
        const btncopy = GM_addElement(installArea, 'button', {
            textContent: " 复制:" + altist + " - " + mvname,
            style:Btnstyle


        });
        btncopy.onclick = function copyCode() {
            GM_setClipboard(altist + " - " + mvname, "text");//复制、将剪贴板的文本设置为指定的值
            style:Btnstyle
        }

        //搜索MV名字
        const Searchmvnamet = GM_addElement(installArea, 'button', {
            textContent: " 搜索:" + mvname,
            style:Btnstyle
        });
        Searchmvnamet.onclick = function () {
            window.location.href = SearchUrl + mvname
        }

        //搜索歌手
        const Searchaltist = GM_addElement(installArea, 'button', {
            textContent: " 搜索:" + altist,
            style:Btnstyle
        });
        Searchaltist.onclick = function () {
            if (From == "kuwo"){
                window.location.href = "https://bd.kuwo.cn/search/singers?key=" + altist
            }
            else {
                window.location.href = SearchUrl + altist
            }
        }

        //酷狗 酷我 反向搜索
        const KugouKuwo = GM_addElement(installArea, 'button', {
            textContent: " 酷狗&酷我:" + mvname,
            style:Btnstyle
        });
        KugouKuwo.onclick = function () {
            if (From == "kuwo"){
                window.location.href = "https://www.kugou.com/yy/html/search.html#searchType=mv&searchKeyWord=" + mvname
            }
            else {
                window.location.href = "https://bd.kuwo.cn/search/mv?key=" + mvname
            }
        }

        /*
         //调用 浏览器 下载
        const DownBtnB = GM_addElement(installArea, 'button', {
            textContent: " 点击下载",
            id:"DownBtnB",
            style:Btnstyle

        });
        DownBtnB.onclick = function () {
            document.querySelector("#DownBtnB").textContent="正在下载..."
            // 发送 GET 请求以模拟下载
            GM_xmlhttpRequest({
                method: "GET",
                url: mvurl,
                responseType: "blob", // 设置响应类型为二进制数据
                onload: function(response) {
                    // 创建临时链接并模拟点击下载
                    var url = URL.createObjectURL(response.response);
                    var a = document.createElement("a");
                    a.href = url;
                    a.download = altist + " - " + mvname;
                    document.body.appendChild(a);
                    a.click();
                    // 下载完成后清除临时链接
                    URL.revokeObjectURL(url);
                    // 移除临时链接元素
                    document.body.removeChild(a);
                }
            });

        }
        */


    }






})();